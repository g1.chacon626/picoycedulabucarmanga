import React , { useState }  from 'react';
import Navbar from './components/navbar';
import Hero from './components/hero';
import SearchDigit from './components/searchDigit';
function App() {
  const [region, setregion] = useState('');
  const changeRegion = ( region_send )=> {
    const region = region_send;
    setregion( region )
  }
  console.log("region desde el parent" + region );
  return (
    <div>
      <Navbar changeRegion={ changeRegion }>
      </Navbar>
      <Hero></Hero>
      <SearchDigit></SearchDigit>
    </div>
  );
}

export default App;
