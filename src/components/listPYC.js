import React , { useState  , useEffect } from 'react';
import './styles/listPYC.css'
import logic_date from './logic_date'; //logica para manejar las fechas


//********* componente para listar los dias de la semana que con sus respectivos digitos de pico y cedula. *****
const DaysWeek = ( { messageList , dayWeek , number_day } ) => {
    const [last_numbers, setlast_numbers] = useState('')
    useEffect(() => {
        const last_number_fn =  logic_date.CalculateLastNumber(number_day);
        const Dayweek_fn = logic_date.dayIsoWeek();
        console.log("isoweekday : " , Dayweek_fn); 
        setlast_numbers( last_number_fn )
    }, [])
    console.log("renderizando DaysWeek");
       
    return (
        <div class="tab-pane fade" id={`list-${dayWeek}`} role="tabpanel" aria-labelledby={`list-${dayWeek}-list`}>
            { messageList } {last_numbers}  
        </div>
        
    )
}



const ListPYC = ( {since , until } ) => {    
    const days_week  = ['monday','tuesday','wednesday','thursday','friday','saturday'];
    return (
        <div className="container_list">
            <div className="listPYC">
                <div>
                    <h5>
                        <b>DESDE</b> <i> { since }</i> <b>HASTA</b> <i>{ until }</i> 
                    </h5>
                    </div>
                <div class="row">
                    <div class="col-4">
                        <div class="list-group" id="list-tab" role="tablist">
                        <a class="list-group-item list-group-item-action" id="list-monday-list" data-toggle="list" href="#list-monday" role="tab" aria-controls="home">Lunes</a>
                        <a class="list-group-item list-group-item-action" id="list-tuesday-list" data-toggle="list" href="#list-tuesday" role="tab" aria-controls="profile">Martes</a>
                        <a class="list-group-item list-group-item-action" id="list-wednesday-list" data-toggle="list" href="#list-wednesday" role="tab" aria-controls="messages">Miercoles</a>
                        <a class="list-group-item list-group-item-action" id="list-thursday-list" data-toggle="list" href="#list-thursday" role="tab" aria-controls="settings">Jueves</a>
                        <a class="list-group-item list-group-item-action" id="list-friday-list" data-toggle="list" href="#list-friday" role="tab" aria-controls="settings">Viernes</a>
                        <a class="list-group-item list-group-item-action" id="list-saturday-list" data-toggle="list" href="#list-saturday" role="tab" aria-controls="settings">Sabado</a>
                        </div>
                    </div>
                    <div class="col-8">
                        <div class="tab-content" id="nav-tabContent">
                            { days_week.map( day_week => (
                                <DaysWeek
                                dayWeek={ day_week }
                                number_day={1}
                                messageList={'Cedulas terminadas en : '}
                                >
                                </DaysWeek>
                                ))}
                        </div>    
                    </div>
                </div> 
            </div>
            
        </div>
        
    )
}
export default ListPYC;