import moment from 'moment';
//CALCULAR LOS UTIMOS DIAS DIGITOS
const CalculateLastNumber = ( day ) => {
    const blockImpar = "1 , 3 , 5 ,7 , 9";
    const blockPar = "0 , 2 , 4 , 6 , 8 ";
    if(( parseInt(day) % 2 ) === 0  ){
        return blockPar;
    }
    return blockImpar;
}
const calculateParAndImpar = ( day ) => {
    if(( parseInt(day) % 2 ) === 0  ){
        return 1; //par
    }
    return 0; // impar
}
const isPYC = (lastDigit  , day ) => {
    const validatedDay = calculateParAndImpar( day )
    const validatedLastDigit = calculateParAndImpar( lastDigit )
    if( validatedDay ==  validatedLastDigit ){
        return 1; //IS PYC 
    }else {
        return 0; //IS NOT PYC
    }
}


//CALCULAR LOS EL DIA ACTUAL
const calculateDayNow = () => {
    var now = moment().format('D');
    return now;
}
//CALCULAR SI EL DIA CUMPLE CON PICO Y CEDULA
const validateAvalibleDayPYC = () => {
    console.log("test" , moment().isoWeekday());
    if(parseInt(moment().isoWeekday()) !== 7 ) {
        return true;
    }
    return false; 
}
const dayIsoWeek = () => {
    return moment().isoWeekday();
}

export default { CalculateLastNumber , 
                 calculateDayNow , 
                 validateAvalibleDayPYC , 
                 dayIsoWeek , 
                 calculateParAndImpar,
                 isPYC
                };
