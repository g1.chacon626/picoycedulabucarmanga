import React , { useEffect , useState } from 'react';
import './styles/searchDigit.css';
import Modal from './modal';
import logic_date from './logic_date'; //logica para manejar las fechas


const SearchDigit = () => {
    const [digit, setdigit] = useState('none')
    const [message, setmessage] = useState('No se ha especificado digito')
    const [validateInput, setvalidateInput] = useState( true );

    useEffect (() => {
        console.log("ejecutando el useEffect" , digit);
        if(digit != 'none'){
            const day = logic_date.calculateDayNow();
            const isPYC = logic_date.isPYC(digit , day );
            if( isPYC == 1){
                setmessage ("Hoy es tu pico y cedula");
            }else {
                setmessage ("Hoy NO es tu pico y cedula");
            }
        }else {
            setmessage ("No se ha especificado digito");
        }
        
    },[ digit ])

    const handlerSearch = (e) => {
        
        const digit_input = e.target.value;
        if( !isNaN(digit_input) ){
            console.log( "--> " + digit_input);
            if( digit_input.toString().length == 1 ){
                setdigit( digit_input );
                setvalidateInput(true)  
            }   else if (digit_input == ""){
                setvalidateInput(true)
                setdigit( 'none' );
                }else {
                    console.log("dos digitos") 
                    setvalidateInput(false)
                }

            }
    }
    console.log("renderizando search ")

    return (
        <div className="parent_container">
            <div className="container_search">
                <div className="input-group mb-3">
                    <div className="input-group-prepend">
                        {/* <button className="btn btn-outline-secondary" type="button" id="button-addon1">Ultimo Digito</button> */}
                    </div>
                    <input type="text" 
                           class="form-control" 
                           placeholder="Aqui ultimo digito cedula" 
                           aria-label="Example text with button addon" 
                           aria-describedby="button-addon1"
                           onChange={handlerSearch}/>
                </div>
                {!validateInput && (
                        <div class="alert alert-danger" role="alert">
                            Solo se permite un digito
                        </div>
                    )
                }
                {validateInput && (
                        <Modal message={ message } title={'Validacion Digito'} title_btn={'Validar Digito'}> </Modal>
                    )
                }
            </div>
        </div>
    )
}
export default SearchDigit;