import React from 'react';

const Navbar = ( { changeRegion } ) => {
    const setRegion =( e ) => {
        e.preventDefault();
        var region = e.target.getAttribute('codigocity');
        console.log("cambiando de region" , region );
        changeRegion( region );
    }
    return (
        <div>
            <ul className="nav nav-tabs">
                <li className="nav-item" key={'title'}>
                    <a className="nav-link active" href="#"><b>TU PICO Y CEDULA</b></a>
                </li>
                <li className="nav-item dropdown" key={'list_region'}>
                    <a className="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">REGION</a>
                    <div className="dropdown-menu">
                    <a 
                    className="dropdown-item" 
                    href="" 
                    onClick={ setRegion }
                    codigocity="68001">
                        Bucaramanga
                    </a>

                    <a className="dropdown-item" 
                    href="" 
                    onClick={ setRegion } 
                    codigocity="68615">
                        Rionegro
                    </a>
                    {/* <a className="dropdown-item" href="">Something else here</a>
                    <div className="dropdown-divider"></div>
                    <a className="dropdown-item" href="">Separated link</a> */}
                    </div>
                </li>
                <li className="nav-item">
                    <a className="nav-link" id="more_info" href="https://www.who.int/es/emergencies/diseases/novel-coronavirus-2019/advice-for-public" target="_blank">Información OMS</a>
                </li>
                {/* <li className="nav-item">
                    <a className="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
                </li> */}
            </ul>
        </div>
    )
}
export default Navbar