import React , { useEffect } from 'react';
import './styles/modal.css';
const Modal = ({ message , title , title_btn }) => {
    return (
        <div>
            <div className="container_parent">
                <div className="container_modal">
                    {/* <!-- Button trigger modal --> */}
                    <button type="button" className="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    { title_btn }
                    </button>

                    {/* <!-- Modal --> */}
                    <div className="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                        <div className="modal-header">
                            <h4 className="modal-title" id="exampleModalLabel">{ title }</h4>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <h3> { message } </h3>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            {/* <button type="button" className="btn btn-primary">Save changes</button> */}
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            
        </div>
    )
}
export default Modal;