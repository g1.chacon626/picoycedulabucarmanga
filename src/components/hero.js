import React from 'react';
import './styles/hero.css';
import imgHero from '../components/res/img_hero.svg';

const Hero = () => {
    return (
        <div className="containerHero">
               <img src={imgHero}  className="img_hero" alt="imagen localizacion"/>
               <h1>VALIDA ACA TU PICO Y CEDULA</h1>
               <span>Actualmente solo esta habilitado para las regiones de Bucaramanga y Rionegro </span> <br/>
               <span>Estamos trabajando para traerles muchas mas regiones </span>
        </div>
    )
}
export default Hero;